package by.training.task.service;

import by.training.task.dao.FeedbackDAO;
import by.training.task.dao.impl.FeedbackDAOImpl;
import by.training.task.entity.Feedback;
import by.training.task.exceptions.DAOException;
import by.training.task.exceptions.ServiceException;

import java.util.Collection;

/**
 * Created by Nik on 28.11.2015.
 */
public class FeedbackService {
    private FeedbackDAO feedbackDAO = FeedbackDAOImpl.getInstance();
    private static FeedbackService instance = new FeedbackService();

    private FeedbackService(){

    }

    public static FeedbackService getInstance() {
        return instance;
    }

    public boolean createNew(Long studentId, Long trainingId, String feedback, int mark) throws ServiceException {
        Feedback temp = new Feedback(studentId, trainingId, feedback, mark);
        try{
            return this.feedbackDAO.save(temp);
        }catch (DAOException e){
            throw new ServiceException("Error while FeedbackService.createNew()" + e);
        }
    }

    public Collection<Feedback> getAllByStudentId(Long studentId) throws ServiceException {
        try{
            return this.feedbackDAO.getAllByStudentId(studentId);
        }catch (DAOException e){
            throw new ServiceException("Error while FeedbackService.getAllByStudentID()" + e);
        }
    }
}
