package by.training.task.service;

import by.training.task.dao.impl.TeacherDAOImpl;
import by.training.task.dao.TeacherDAO;
import by.training.task.entity.Teacher;
import by.training.task.exceptions.DAOException;
import by.training.task.exceptions.ServiceException;

import java.util.Collection;

/**
 * Created by Nik on 11.11.2015.
 */
public class TeacherService {
    private TeacherDAO teacherDAO = TeacherDAOImpl.getInstance();
    private static TeacherService instance = new TeacherService();

    private TeacherService(){

    }

    public static TeacherService getInstance() {
        return instance;
    }

    public Collection<Teacher> getAll() throws ServiceException {
        try{
            return this.teacherDAO.getAll();
        }catch (DAOException e){
            throw new ServiceException("Error while TeacherService.getAll() " + e);
        }
    }

    public boolean createNew(Teacher teacher) throws ServiceException {
        try{
            return this.teacherDAO.save(teacher);
        }catch (DAOException e){
            throw new ServiceException("Error while TeacherService.createNew() " + e);
        }
    }

    public Teacher getById(Long teacherId) throws ServiceException {
        try{
            return this.teacherDAO.getById(teacherId);
        }catch (DAOException e){
            throw new ServiceException("Error while TeacherService.getById() " + e);
        }
    }

}
