package by.training.task.service;

import by.training.task.dao.impl.TrainingDAOImpl;
import by.training.task.dao.TrainingDAO;
import by.training.task.entity.Teacher;
import by.training.task.entity.Training;
import by.training.task.exceptions.DAOException;
import by.training.task.exceptions.ServiceException;

import java.util.Collection;
import java.util.Date;

/**
 * Created by Nik on 09.11.2015.
 */
public class TrainingService {
    private TrainingDAO trainingDAO = TrainingDAOImpl.getInstance();
    private static TrainingService instance = new TrainingService();

    private TrainingService(){

    }

    public static TrainingService getInstance() {
        return instance;
    }

    public Collection<Training> getAll() throws ServiceException {
        try{
            return this.trainingDAO.getAll();
        }catch (DAOException e){
            throw new ServiceException("Error while TrainingService.getAll() " + e);
        }
    }

    public Collection<Training> getAllFinished() throws ServiceException {
        try{
            return this.trainingDAO.getAllFinished();
        }catch (DAOException e){
            throw new ServiceException("Error while TrainingService.getAllFinished() " + e);
        }
    }

    public boolean createNew(String name, Date startDate, Date endDate, Teacher teacher) throws ServiceException {
        Training training = new Training();
        training.setName(name);
        training.setStartDate(startDate);
        training.setEndDate(endDate);
        training.setTeacher(teacher);

        try{
            return this.trainingDAO.save(training);
        }catch (DAOException e){
            throw new ServiceException("Error while TrainingService.createNew() " + e);
        }
    }

    public Training getById(Long trainingId) throws ServiceException {
        try{
            return this.trainingDAO.getByTrainingId(trainingId);
        }catch (DAOException e){
            throw new ServiceException("Error while TrainingService.getById() " + e);
        }
    }

    public boolean finishTraining(Long trainingId) throws ServiceException {
        try{
            return this.trainingDAO.finishTraining(trainingId);
        }catch (DAOException e){
            throw new ServiceException("Error while TrainingService.finishTraining()" + e);
        }
    }

}
