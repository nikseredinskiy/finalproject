package by.training.task.service;

import by.training.task.dao.impl.StudentDAOImpl;
import by.training.task.dao.StudentDAO;
import by.training.task.entity.Student;
import by.training.task.exceptions.DAOException;
import by.training.task.exceptions.ServiceException;

import java.util.Collection;

/**
 * Created by Nik on 02.11.2015.
 */
public class StudentService {
    private StudentDAO studentDAO = StudentDAOImpl.getInstance();
    private static StudentService instance = new StudentService();

    private StudentService(){

    }

    public static StudentService getInstance() {
        return instance;
    }

    public Collection<Student> getAll() throws ServiceException {
        try{
            return this.studentDAO.getAll();
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.getAll()" + e);
        }
    }

    public boolean createNew(Student student) throws ServiceException {
        try{
            return this.studentDAO.save(student);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.createNew()" + e);
        }
    }


    public void update(long studentId, String name, String secondName, String password, String university, int course) throws ServiceException {
        try{
            Student old = getById(studentId);
            old.setName(name);
            old.setSecondName(secondName);
            old.setPassword(password);
            old.setUniversity(university);
            old.setCourse(course);

            this.studentDAO.update(old);
        } catch (DAOException e){
            throw new ServiceException("Error while StudentService.update()" + e);
        }
    }

    public boolean delete(Long studentId) throws ServiceException {
        try{
            return this.studentDAO.delete(studentId);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.delete()" + e);
        }
    }

    public boolean registerToTraining(Long studentId, Long trainingId) throws ServiceException {
        try{
            return this.studentDAO.registerToTraining(studentId, trainingId);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.registerToTraining()" + e);
        }
    }

    public boolean deleteFromTraining(Long studentId, Long trainingId) throws ServiceException{
        try{
            return this.studentDAO.deleteFromTraining(studentId, trainingId);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.deleteFromTraining()" + e);
        }
    }

    public Student getByEmail(String mail) throws ServiceException {
        try{
            return this.studentDAO.getByEmail(mail);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.getByEmail()" + e);
        }
    }

    public Student getById(long studentId) throws ServiceException {
        try{
            return this.studentDAO.getById(studentId);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.getById()" + e);
        }
    }

    public String checkLogin(String enterLogin, String enterPass) throws ServiceException {
        try{
            return this.studentDAO.checkLogin(enterLogin, enterPass);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.checkLogin()" + e);
        }
    }

    public Collection<Student> getOnTraining(Long trainingId) throws ServiceException {
        try{
            return this.studentDAO.getOnTraining(trainingId);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.getOnTraining()" + e);
        }
    }

    public Collection<Student> getArchive() throws ServiceException{
        try{
            return this.studentDAO.getArchive();
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.getArchive()" + e);
        }
    }

    public boolean isOnTraining(Long studentId, Long trainingId) throws ServiceException{
        try{
            return this.studentDAO.isOnTraining(studentId, trainingId);
        }catch (DAOException e){
            throw new ServiceException("Error while StudentService.isOnTraining()" + e);
        }
    }
}
