package by.training.task.controller;

import by.training.task.command.ActionCommand;
import by.training.task.command.ActionFactory;
import by.training.task.command.ConfigurationManager;
import by.training.task.exceptions.ServiceException;
import by.training.task.service.FeedbackService;
import by.training.task.service.StudentService;
import by.training.task.service.TeacherService;
import by.training.task.service.TrainingService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Nik on 04.11.2015.
 */

@WebServlet("/controller")
public class Controller extends HttpServlet {

    private static final String CONFIG_PATH = "/config/log4j.xml";
    private static Logger logger = Logger.getLogger(Controller.class);

    @Override
    public void init() {
        String realConfiguration = getServletContext().getRealPath(CONFIG_PATH);
        new DOMConfigurator().doConfigure(realConfiguration, LogManager.getLoggerRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = null;
        ActionCommand command = ActionFactory.defineCommand(request);

        page = command.execute(request);

        if (request.getSession().getAttribute("role") == null) {
            request.getSession().setAttribute("role", "GUEST");
            page = ConfigurationManager.getProperty("path.page.lists");
        }

        if(!getLists(request)){
            page = ConfigurationManager.getProperty("path.page.error");
        }

        if (page != null) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);

            dispatcher.forward(request, response);
        } else {
            page = ConfigurationManager.getProperty("path.page.lists");
            response.sendRedirect(request.getContextPath() + page);
        }
    }

    private boolean getLists(HttpServletRequest request){
        boolean flag = false;

        StudentService studentService = StudentService.getInstance();
        TrainingService trainingService = TrainingService.getInstance();
        TeacherService teacherService = TeacherService.getInstance();
        FeedbackService feedbackService = FeedbackService.getInstance();

        try {
            request.setAttribute("students", studentService.getAll());
            request.setAttribute("trainings", trainingService.getAll());
            request.setAttribute("teachers", teacherService.getAll());
            request.setAttribute("archive", studentService.getArchive());
            request.setAttribute("finishedTrainings", trainingService.getAllFinished());

            if ("STUDENT".equals(request.getSession().getAttribute("role"))) {
                String stId = String.valueOf(request.getSession().getAttribute("currentStudentId"));
                Long studentId = Long.parseLong(stId);
                request.setAttribute("currStudentFeedbacks", feedbackService.getAllByStudentId(studentId));

            }

            flag = true;
        } catch (ServiceException e) {
            request.setAttribute("errorDescription", e);
            flag = false;
        }
        return flag;

    }
}
