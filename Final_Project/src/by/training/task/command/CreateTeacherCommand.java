package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 11.11.2015.
 */
public class CreateTeacherCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        page = ConfigurationManager.getProperty("path.page.add-teacher");

        return page;
    }
}
