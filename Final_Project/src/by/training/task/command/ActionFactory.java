package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 04.11.2015.
 */
public class ActionFactory {
    public static ActionCommand defineCommand(HttpServletRequest request){
        ActionCommand current = new EmptyCommand();
        String action = request.getParameter("command");

        if(action == null || action.isEmpty()){
            return current;
        }
        try {
            action = action.replace('-','_');
            CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());

            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action + MessageManager.getProperty("message.wrongaction"));
        }
        return current;
    }
}
