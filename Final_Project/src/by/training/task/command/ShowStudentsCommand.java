package by.training.task.command;

import by.training.task.entity.Student;
import by.training.task.exceptions.ServiceException;
import by.training.task.service.StudentService;
import by.training.task.service.TrainingService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * Created by Nik on 23.11.2015.
 */
public class ShowStudentsCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(ShowStudentsCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        StudentService studentService = StudentService.getInstance();
        TrainingService trainingService = TrainingService.getInstance();
        Long trainingId = Long.parseLong(request.getParameter("training_id"));

        Collection<Student> list = null;
        try{
            list = studentService.getOnTraining(trainingId);
            request.setAttribute("studentsOnTraining", list);
            request.setAttribute("training", trainingService.getById(trainingId));

            page = ConfigurationManager.getProperty("path.page.students-on-training");
        }catch (ServiceException e){
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error in ShowStudentsCommand", e);
        }

        return page;
    }
}
