package by.training.task.command;

import by.training.task.entity.Student;
import by.training.task.exceptions.ServiceException;
import by.training.task.safety.Encryption;
import by.training.task.service.StudentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Nik on 04.11.2015.
 */
public class LoginCommand implements ActionCommand {
    private static final String PARAM_NAME_LOGIN = "login";
    private static final String PARAM_NAME_PASSWORD = "password";

    private static Logger logger = Logger.getLogger(LoginCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        StudentService studentService = StudentService.getInstance();

        String page = null;
        String login = request.getParameter(PARAM_NAME_LOGIN);
        String pass = Encryption.codePassword(request.getParameter(PARAM_NAME_PASSWORD));

        try {
            String role = studentService.checkLogin(login, pass);
            if (role != null) {
                HttpSession session = request.getSession(true);
                if ("GUEST".equals(session.getAttribute("role"))) {
                    session.setAttribute("role", role);

                    if("STUDENT".equals(role)){
                        Student temp = studentService.getByEmail(login);
                        session.setAttribute("currentStudentId", temp.getId());
                    }
                }

                page = ConfigurationManager.getProperty("path.page.lists");
            } else {
                request.setAttribute("errorLoginPassMessage", MessageManager.getProperty("message.loginerror"));
                page = ConfigurationManager.getProperty("path.page.login");
                logger.error("Wrong Login/Password while LoginCommand");

            }
        }catch (ServiceException e){
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error while LoginCommand", e);
        }
        return page;
    }
}
