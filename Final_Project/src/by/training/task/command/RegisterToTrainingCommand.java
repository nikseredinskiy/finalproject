package by.training.task.command;

import by.training.task.exceptions.ServiceException;
import by.training.task.service.StudentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 23.11.2015.
 */
public class RegisterToTrainingCommand implements ActionCommand{

    private static Logger logger = Logger.getLogger(RegisterToTrainingCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        StudentService studentService = StudentService.getInstance();
        Long trainingId = Long.parseLong(request.getParameter("trainingId"));
        Long studentId = Long.parseLong(String.valueOf(request.getSession().getAttribute("currentStudentId")));

        try{
            if(studentService.registerToTraining(studentId, trainingId)){
                page = ConfigurationManager.getProperty("path.page.lists");
            }
        }catch (ServiceException e){
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error while RegisterToTrainingCommand", e);
        }

        return page;
    }
}
