package by.training.task.command;

import java.util.ResourceBundle;

/**
 * Created by Nik on 04.11.2015.
 */
public class MessageManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.messages");
    private MessageManager() { }
    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
