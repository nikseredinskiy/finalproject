package by.training.task.command;

import by.training.task.entity.Feedback;
import by.training.task.exceptions.ServiceException;
import by.training.task.service.FeedbackService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * Created by Nik on 28.11.2015.
 */
public class ShowFeedbacksCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(ShowFeedbacksCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        FeedbackService feedbackService = FeedbackService.getInstance();

        Long studentId = Long.parseLong(request.getParameter("studentId"));
        Collection<Feedback> list = null;
        try{
            list = feedbackService.getAllByStudentId(studentId);
            request.setAttribute("studentFeedbacks", list);
            page = ConfigurationManager.getProperty("path.page.student-feedbacks");
        }catch (ServiceException e){
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error in ShowFeedbackCommand");
        }
        return page;
    }
}
