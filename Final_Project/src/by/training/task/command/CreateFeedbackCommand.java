package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 28.11.2015.
 */
public class CreateFeedbackCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        Long studentId = Long.parseLong(request.getParameter("feedbackStudentId"));
        Long trainingId = Long.parseLong(request.getParameter("training_Id"));

        request.setAttribute("stId", studentId);
        request.setAttribute("trId", trainingId);

        page = ConfigurationManager.getProperty("path.page.add-feedback");

        return page;
    }
}
