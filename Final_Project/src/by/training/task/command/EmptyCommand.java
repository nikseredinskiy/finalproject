package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 04.11.2015.
 */
public class EmptyCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getProperty("path.page.lists");
        return page;
    }
}
