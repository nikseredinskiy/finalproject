package by.training.task.command;

/**
 * Created by Nik on 04.11.2015.
 */
public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    SIGN_UP{
        {
            this.command = new SignUpCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    NEW_STUDENT {
        {
            this.command = new CreateStudentCommand();
        }
    },
    ADD_STUDENT {
        {
            this.command = new AddStudentCommand();
        }
    },
    NEW_TRAINING {
        {
            this.command = new CreateTrainingCommand();
        }
    },
    ADD_TRAINING {
        {
            this.command = new AddTrainingCommand();
        }
    },
    NEW_TEACHER {
        {
            this.command = new CreateTeacherCommand();
        }
    },
    ADD_TEACHER {
        {
            this.command = new AddTeacherCommand();
        }
    },
    UPDATE_STUDENT {
        {
            this.command = new UpdateStudentCommand();
        }
    },
    DELETE_STUDENT {
        {
            this.command = new DeleteStudentCommand();
        }
    },
    REGISTER_TO_TRAINING {
        {
            this.command = new RegisterToTrainingCommand();
        }
    },
    DELETE_FROM_TRAINING {
        {
            this.command = new DeleteFromTrainingCommand();
        }
    },
    SHOW_STUDENTS{
        {
            this.command = new ShowStudentsCommand();
        }
    },
    NEW_FEEDBACK{
        {
            this.command = new CreateFeedbackCommand();
        }
    },
    ADD_FEEDBACK{
        {
            this.command = new AddFeedbackCommand();
        }
    },
    SHOW_FEEDBACK{
        {
            this.command = new ShowFeedbacksCommand();
        }
    },
    CHANGE_LOCALE{
        {
            this.command = new ChangeLocaleCommand();
        }
    },
    BACK{
        {
            this.command = new BackCommand();
        }
    },
    FINISH_TRAINING{
        {
            this.command = new FinishTrainingCommand();
        }
    };

    ActionCommand command;
    public ActionCommand getCurrentCommand() {
        return command;
    }
}
