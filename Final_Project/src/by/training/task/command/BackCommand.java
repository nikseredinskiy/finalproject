package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 07.12.2015.
 */
public class BackCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request){
        String page = null;

        page = ConfigurationManager.getProperty("path.page.lists");
        return page;
    }
}
