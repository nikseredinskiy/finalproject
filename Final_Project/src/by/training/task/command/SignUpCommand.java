package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 01.12.2015.
 */
public class SignUpCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        page = ConfigurationManager.getProperty("path.page.login");

        return page;
    }
}
