package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 28.11.2015.
 */
public class ChangeLocaleCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        String locale = null;
        String loc = request.getParameter("locale");

        switch (loc.toLowerCase()) {
            case "eng":
                locale = "en_US";
                break;
            case "rus":
                locale = "ru_RU";
                break;
        }

        request.getSession().setAttribute("locale", locale);

        page = ConfigurationManager.getProperty("path.page.lists");
        return page;
    }
}
