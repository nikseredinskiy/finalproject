package by.training.task.command;

import java.util.ResourceBundle;

/**
 * Created by Nik on 04.11.2015.
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");

    private ConfigurationManager(){}
    public static String getProperty(String key){
        return resourceBundle.getString(key);
    }
}
