package by.training.task.command;


import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 04.11.2015.
 */
public interface ActionCommand {
    String execute(HttpServletRequest request);
}
