package by.training.task.command;

import by.training.task.entity.Teacher;
import by.training.task.exceptions.ServiceException;
import by.training.task.service.TeacherService;
import by.training.task.service.TrainingService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Nik on 09.11.2015.
 */
public class AddTrainingCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(AddTrainingCommand.class);


    @Override
    public String execute(HttpServletRequest request){
        String page = null;
        TrainingService trainingService = TrainingService.getInstance();
        TeacherService teacherService = TeacherService.getInstance();

        try {
            String name = request.getParameter("name");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            Date startDate = format.parse(request.getParameter("startDate"));
            Date endDate = format.parse(request.getParameter("endDate"));
            Long teacherId = Long.parseLong(request.getParameter("teacherId"));

            boolean isCreated = false;
            try{
                Teacher teacher = teacherService.getById(teacherId);
                isCreated = trainingService.createNew(name, startDate, endDate, teacher);
            }catch (ServiceException e){
                request.setAttribute("errorDescription", e);
                page = ConfigurationManager.getProperty("path.page.error");
                logger.error("Error in AddTrainingCommand", e);
            }

            if (isCreated) {
                page = ConfigurationManager.getProperty("path.page.lists");
            }

        } catch (ParseException e) {
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
        }
        return page;
    }
}
