package by.training.task.command;

import by.training.task.entity.Student;
import by.training.task.exceptions.ServiceException;
import by.training.task.safety.Encryption;
import by.training.task.safety.Validation;
import by.training.task.service.StudentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 04.11.2015.
 */
public class AddStudentCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(AddStudentCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        StudentService studentService = StudentService.getInstance();
        Validation validation = new Validation();

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String secondName = request.getParameter("secondName");
        String email = request.getParameter("email");
        String password = Encryption.codePassword(request.getParameter("password"));
        String university = request.getParameter("university");
        int course = Integer.parseInt(request.getParameter("course"));

        Student student = new Student(name, secondName, email, password, university, course);
        if (validation.validate(student)) {
            try {
                if (id == null) {
                    boolean isCreated = studentService.createNew(student);
                    if (isCreated) {
                        page = ConfigurationManager.getProperty("path.page.lists");
                    }
                } else {
                    studentService.update(Long.parseLong(id), name, secondName, password, university, course);
                    page = ConfigurationManager.getProperty("path.page.lists");
                }

                if ("GUEST".equals(request.getSession().getAttribute("role"))) {
                    request.getSession().setAttribute("role", "STUDENT");
                    Student temp = studentService.getByEmail(email);
                    request.getSession().setAttribute("currentStudentId", temp.getId());
                }

            } catch (ServiceException e) {
                request.setAttribute("errorDescription", e);
                page = ConfigurationManager.getProperty("path.page.error");
                logger.error("Error in AddStudentCommand", e);
            }
        } else {
            request.setAttribute("errorDescription", "Validation Error");
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error while validation in AddStudentCommand");
        }

        return page;
    }
}
