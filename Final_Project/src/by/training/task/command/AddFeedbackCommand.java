package by.training.task.command;

import by.training.task.exceptions.ServiceException;
import by.training.task.service.FeedbackService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 28.11.2015.
 */
public class AddFeedbackCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(AddFeedbackCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        FeedbackService feedbackService = FeedbackService.getInstance();

        Long studentId = Long.parseLong(request.getParameter("stId"));
        Long trainingId = Long.parseLong(request.getParameter("trId"));

        String text = request.getParameter("feedbackText");
        int mark = Integer.parseInt(request.getParameter("feedbackMark"));

        boolean isCreated = false;
        try {
            isCreated = feedbackService.createNew(studentId, trainingId, text, mark);
        } catch (ServiceException e){
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error in AddFeedbackCommand", e);
        }
        if (isCreated) {
            page = ConfigurationManager.getProperty("path.page.lists");
        }

        return page;
    }
}
