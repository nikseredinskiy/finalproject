package by.training.task.command;

import by.training.task.exceptions.ServiceException;
import by.training.task.service.StudentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 12.11.2015.
 */
public class DeleteStudentCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(DeleteStudentCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        StudentService studentService = StudentService.getInstance();
        Long studentId = Long.parseLong(request.getParameter("studentDeleteId"));

        try{
            if(studentService.delete(studentId)){
                page = ConfigurationManager.getProperty("path.page.lists");
            }
        }catch (ServiceException e){
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error in DeleteStudentCommand", e);
        }

        return page;
    }
}
