package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 09.11.2015.
 */
public class CreateTrainingCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        page = ConfigurationManager.getProperty("path.page.add-training");

        return page;
    }
}
