package by.training.task.command;

import by.training.task.exceptions.ServiceException;
import by.training.task.service.TrainingService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 12.12.2015.
 */
public class FinishTrainingCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(FinishTrainingCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        TrainingService trainingService = TrainingService.getInstance();
        Long trainingId = Long.parseLong(request.getParameter("training_id"));

        try{
            if(trainingService.finishTraining(trainingId)){
                page = ConfigurationManager.getProperty("path.page.lists");
            }
        }catch (ServiceException e){
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error in FinishTrainingCommand", e);
        }

        return page;
    }
}
