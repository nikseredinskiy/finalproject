package by.training.task.command;

import by.training.task.entity.Teacher;
import by.training.task.exceptions.ServiceException;
import by.training.task.safety.Encryption;
import by.training.task.safety.Validation;
import by.training.task.service.TeacherService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 11.11.2015.
 */
public class AddTeacherCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(AddTeacherCommand.class);

    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        TeacherService teacherService = TeacherService.getInstance();
        Validation validation = new Validation();

        String name = request.getParameter("name");
        String secondName = request.getParameter("secondName");
        String email = request.getParameter("email");
        String password = Encryption.codePassword(request.getParameter("password"));

        Teacher teacher = new Teacher(name, secondName, email, password);

        boolean isCreated = false;
        if (validation.validate(teacher)) {
            try {
                isCreated = teacherService.createNew(teacher);
            } catch (ServiceException e) {
                request.setAttribute("errorDescription", e);
                page = ConfigurationManager.getProperty("path.page.error");
                logger.error("Error while AddTeacherCommand", e);
            }

            if (isCreated) {
                page = ConfigurationManager.getProperty("path.page.lists");
            }

        }else{
            request.setAttribute("errorDescription", "Validation Error");
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error while AddTeacherCommand validation");
        }

        return page;
    }
}
