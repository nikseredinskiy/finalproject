package by.training.task.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 04.11.2015.
 */
public class CreateStudentCommand implements ActionCommand {
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        page = ConfigurationManager.getProperty("path.page.add-student");

        return page;
    }
}
