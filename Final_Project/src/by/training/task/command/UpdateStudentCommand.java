package by.training.task.command;

import by.training.task.entity.Student;
import by.training.task.exceptions.ServiceException;
import by.training.task.service.StudentService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Nik on 12.11.2015.
 */
public class UpdateStudentCommand implements ActionCommand {

    private static Logger logger = Logger.getLogger(UpdateStudentCommand.class);
    
    @Override
    public String execute(HttpServletRequest request) {
        String page = null;
        StudentService studentService = StudentService.getInstance();
        Long studentId = Long.parseLong(request.getParameter("studentEditId"));

        Student student = null;
        try{
            student = studentService.getById(studentId);
            request.setAttribute("studentForEdit", student);
            page = ConfigurationManager.getProperty("path.page.update-student");

        }catch (ServiceException e){
            request.setAttribute("errorDescription", e);
            page = ConfigurationManager.getProperty("path.page.error");
            logger.error("Error in UpdateStudentCommand", e);
        }

        return page;
    }
}
