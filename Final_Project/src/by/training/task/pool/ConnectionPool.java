package by.training.task.pool;

/**
 * Created by Nik on 16.11.2015.
 */
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import static by.training.task.pool.DBPropertyManager.getProperty;


public class ConnectionPool {
    private static AtomicBoolean bool = new AtomicBoolean();
    private static ConnectionPool instance;
    private static ReentrantLock lock = new ReentrantLock();
    private LinkedBlockingQueue<ProxyConnection> connectionQueue;
    private final static String URL = getProperty("url");
    private final static String USER = getProperty("user");
    private final static String PASSWORD = getProperty("password");
    private final static int POOL_SIZE = Integer.parseInt(getProperty("poolSize"));

    private static Logger logger = Logger.getLogger(ConnectionPool.class);

    private ConnectionPool(int poolSize) {
        connectionQueue = new LinkedBlockingQueue(poolSize);
        for (int i = 0; i < poolSize; i++) {
            ProxyConnection connection = createConnection();
            connectionQueue.offer(connection);
        }
    }

    private ProxyConnection createConnection() {
        ProxyConnection px = null;
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);

            px = new ProxyConnection(connection);
        } catch (SQLException e) {
            logger.error("ConnectionPool.createConnection()", e);
        }
        return px;
    }

    public static ConnectionPool getInstance() {
        if (!bool.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool(POOL_SIZE);
                    bool = new AtomicBoolean(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public ProxyConnection getConnection() {
        ProxyConnection connection = null;
        try {
            connection = connectionQueue.take();
        } catch (InterruptedException e) {
            logger.error(e);
        }
        return connection;
    }

    public void closeConnection(ProxyConnection connection) {
        try {
            if (!connection.getAutoCommit()) {
                connection.setAutoCommit(true);
            }
            connectionQueue.put(connection);
        } catch (SQLException | InterruptedException e) {
            logger.error(e);
        }
    }

    public void closeAll() {
        connectionQueue.forEach(ProxyConnection::doClose);
    }
}
