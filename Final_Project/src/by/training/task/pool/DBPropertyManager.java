package by.training.task.pool;

import java.util.ResourceBundle;

/**
 * Created by Nik on 16.11.2015.
 */
public class DBPropertyManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("resources.config");

    private DBPropertyManager() {

    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
