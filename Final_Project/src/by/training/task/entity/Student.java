package by.training.task.entity;

/**
 * Created by Nik on 02.11.2015.
 */
public class Student {
    private Long id;
    private String name;
    private String secondName;
    private String email;
    private String password;
    private String university;
    private int course;

    public Student(){

    }

    public Student(String name, String secondName, String email, String university) {
        this.name = name;
        this.secondName = secondName;
        this.email = email;
        this.university = university;
    }

    public Student(String name, String secondName, String email, String password, String university, int course) {
        this.name = name;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
        this.university = university;
        this.course = course;
    }

    public Student(Long id, String name, String secondName, String email, String password, String university, int course) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.email = email;
        this.password = password;
        this.university = university;
        this.course = course;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
     public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", university='" + university + '\'' +
                ", course=" + course +
                '}';
    }
}
