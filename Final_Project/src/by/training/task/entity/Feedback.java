package by.training.task.entity;

/**
 * Created by Nik on 28.11.2015.
 */
public class Feedback {
    private Long id;
    private Long studentId;
    private Long trainingId;
    private String trainingName;
    private String feedback;
    private int mark;


    public Feedback(Long id, Long studentId, Long trainingId, String feedback, int mark) {
        this.id = id;
        this.studentId = studentId;
        this.trainingId = trainingId;
        this.feedback = feedback;
        this.mark = mark;
    }

    public Feedback(Long studentId, Long trainingId, String feedback, int mark) {
        this.studentId = studentId;
        this.trainingId = trainingId;
        this.feedback = feedback;
        this.mark = mark;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(Long trainingId) {
        this.trainingId = trainingId;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getTrainingName() {
        return trainingName;
    }

    public void setTrainingName(String trainingName) {
        this.trainingName = trainingName;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }
}
