package by.training.task.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Nik on 30.11.2015.
 */
@SuppressWarnings("serial")
public class StudentListTag extends TagSupport {
    private String role;
    private String studentID;

    public void setRole(String role) {
        this.role = role;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    @Override
    public int doStartTag() throws JspException {
        Locale locale = new Locale("ru", "RU");

        if ("en_US".equals(pageContext.getSession().getAttribute("locale"))) {
            locale = new Locale("en", "US");
        }

        ResourceBundle resourceBundle = ResourceBundle.getBundle("pagecontent", locale);
        String showFeedback = resourceBundle.getString("label.show-feedback");
        String edit = resourceBundle.getString("label.edit");
        String remove = resourceBundle.getString("label.remove");
        try {
            String code = null;
            switch (role) {
                case "GUEST":
                    code = "<td>\n" +
                            "                    </td>\n" +
                            "                    <td>\n" +
                            "                    </td>\n" +
                            "                    <td>\n" +
                            "                    </td>";
                    break;
                case "STUDENT":
                    code = "<td>\n" +
                            "                    </td>\n" +
                            "                    <td>\n" +
                            "                    </td>\n" +
                            "                    <td>\n" +
                            "                    </td>";
                    break;
                case "TEACHER":
                    code = "<td>\n" +
                            "                        <form action=\"/controller\" id=\"feedbackForm\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"show-feedback\">\n" +
                            "                            <input type=\"hidden\" id=\"studentId\" name=\"studentId\" value=" + studentID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-info\" value= " + showFeedback +
                            "                                   >\n" +
                            "                        </form>\n" +
                            "                    </td>" +
                            "<td></td>" +
                            "<td></td>";
                    break;
                case "ADMINISTRATOR":
                    code = "<td>\n" +
                            "                        <form action=\"/controller\" id=\"feedbackForm\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"show-feedback\">\n" +
                            "                            <input type=\"hidden\" id=\"studentId\" name=\"studentId\" value=" + studentID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-info\" value= " + showFeedback +
                            "                                   >\n" +
                            "                        </form>\n" +
                            "                    </td>\n" +
                            "                    <td>\n" +
                            "                        <form action=\"/controller\" id=\"editForm\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"update-student\">\n" +
                            "                            <input type=\"hidden\" id=\"studentEditId\" name=\"studentEditId\" value=" + studentID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-info\" value=" + edit +
                            "                                   >\n" +
                            "                        </form>\n" +
                            "                    </td>\n" +
                            "                    <td>\n" +
                            "                        <form action=\"/controller\" id=\"deleteForm\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"delete-student\">\n" +
                            "                            <input type=\"hidden\" id=\"studentDeleteId\" name=\"studentDeleteId\" value=" + studentID + ">\n" +
                            "                            <input type=\"submit\" class =\"btn btn-danger\" id=\"remove-button\" value=" + remove +
                            "                                   >\n" +
                            "                        </form>\n" +
                            "                    </td>";
                    break;
            }
            pageContext.getOut().write(code);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

}
