package by.training.task.tag;

import by.training.task.exceptions.ServiceException;
import by.training.task.service.StudentService;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Nik on 30.11.2015.
 */
public class TrainingListTag extends TagSupport {
    private String role;
    private String trainingID;

    public void setRole(String role) {
        this.role = role;
    }

    public void setTrainingID(String trainingID) {
        this.trainingID = trainingID;
    }

    private Logger logger = Logger.getLogger(TrainingListTag.class);

    @Override
    public int doStartTag() throws JspException {
        Locale locale = new Locale("ru", "RU");

        if ("en_US".equals(pageContext.getSession().getAttribute("locale"))) {
            locale = new Locale("en", "US");
        }

        ResourceBundle resourceBundle = ResourceBundle.getBundle("pagecontent", locale);
        String studentsOnTraining = resourceBundle.getString("label.students-on-training");
        String register = resourceBundle.getString("label.register");
        String cancel = resourceBundle.getString("label.cancel-register");
        String singUp = resourceBundle.getString("label.sign-up");
        String finish = resourceBundle.getString("label.finish");

        StudentService studentService = StudentService.getInstance();
        boolean isRegistered = false;

        if ("STUDENT".equals(role)) {
            Long studentId = Long.parseLong(pageContext.getSession().getAttribute("currentStudentId").toString());
            try {
                isRegistered = studentService.isOnTraining(studentId, Long.parseLong(trainingID));
            } catch (ServiceException e) {
                logger.error("Error in TrainingTag", e);
            }
        }

        try {
            String code = null;
            String button = null;
            switch (role) {
                case "GUEST":
                    code = "<td>" +
                            "<form action=\"/controller\" id=\"login\" method=\"post\">\n" +
                            "<input type=\"hidden\" name=\"command\" value=\"sign-up\">" +
                            "<input type=\"submit\" class=\"btn btn-primary\" id=\"sign-button\" value=" +
                            singUp + " ></form>" +
                            "</td>" +
                            "<td></td>";
                    break;
                case "STUDENT":
                    if (isRegistered) {
                        button = "<input type=\"hidden\" name=\"command\" value=\"delete-from-training\">" +
                                "<input type=\"submit\" class=\"btn btn-danger\" id=\"delete-button\" value=" +
                                cancel + " >";
                    } else {
                        button = "<input type=\"hidden\" name=\"command\" value=\"register-to-training\">" +
                                "<input type=\"submit\" class=\"btn btn-primary\" id=\"register-button\" value=" +
                                register + " >";
                    }
                    code = "                    <td>\n" +
                            "                        <form action=\"/controller\" id=\"register\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" id=\"trainingId\" name=\"trainingId\" value=" + trainingID + ">\n"
                                                            + button +
                            "                        </form>\n" +
                            "                    </td>" + "<td></td>";
                    break;
                case "TEACHER":
                    code = "<td>\n" +
                            "                        <form action=\"/controller\" id=\"show_students\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"show-students\">\n" +
                            "                            <input type=\"hidden\" id=\"training_id\" name=\"training_id\" value=" + trainingID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-info\" id=\"show-button\" value=" + studentsOnTraining + " >\n" +
                            "                        </form>\n" +
                            "                    </td>\n" +
                            "<td>\n" +
                            "                        <form action=\"/controller\" id=\"finish_training\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"finish-training\">\n" +
                            "                            <input type=\"hidden\" id=\"training_id\" name=\"training_id\" value=" + trainingID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-danger\" id=\"finish-button\" value=" + finish + " >\n" +
                            "                        </form>\n" +
                            "                    </td>\n";
                    break;
                case "ADMINISTRATOR":
                    code = "<td>\n" +
                            "                        <form action=\"/controller\" id=\"show_students\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"show-students\">\n" +
                            "                            <input type=\"hidden\" id=\"training_id\" name=\"training_id\" value=" + trainingID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-info\" id=\"show-button\" value=" + studentsOnTraining + " >\n" +
                            "                        </form>\n" +
                            "                    </td>\n" +
                            "<td>\n" +
                            "                        <form action=\"/controller\" id=\"finish_training\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"finish-training\">\n" +
                            "                            <input type=\"hidden\" id=\"training_id\" name=\"training_id\" value=" + trainingID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-danger\" id=\"finish-button\" value=" + finish + " >\n" +
                            "                        </form>\n" +
                            "                    </td>\n";
                    break;
            }
            pageContext.getOut().write(code);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }

        return SKIP_BODY;
    }

}
