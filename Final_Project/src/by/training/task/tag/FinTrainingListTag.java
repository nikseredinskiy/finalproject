package by.training.task.tag;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Nik on 12.12.2015.
 */
public class FinTrainingListTag extends TagSupport {
    private String role;
    private String trainingID;

    public void setRole(String role) {
        this.role = role;
    }

    public void setTrainingID(String trainingID) {
        this.trainingID = trainingID;
    }


    @Override
    public int doStartTag() throws JspException {
        Locale locale = new Locale("ru", "RU");

        if ("en_US".equals(pageContext.getSession().getAttribute("locale"))) {
            locale = new Locale("en", "US");
        }

        ResourceBundle resourceBundle = ResourceBundle.getBundle("pagecontent", locale);
        String studentsOnTraining = resourceBundle.getString("label.students-on-training");

        try {
            String code = null;
            switch (role) {
                case "TEACHER":
                    code = "<td>\n" +
                            "                        <form action=\"/controller\" id=\"show_students\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"show-students\">\n" +
                            "                            <input type=\"hidden\" id=\"training_id\" name=\"training_id\" value=" + trainingID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-info\" id=\"show-button\" value=" + studentsOnTraining + " >\n" +
                            "                        </form>\n" +
                            "                    </td>\n";
                    break;
                case "ADMINISTRATOR":
                    code = "<td>\n" +
                            "                        <form action=\"/controller\" id=\"show_students\" method=\"post\">\n" +
                            "                            <input type=\"hidden\" name=\"command\" value=\"show-students\">\n" +
                            "                            <input type=\"hidden\" id=\"training_id\" name=\"training_id\" value=" + trainingID + ">\n" +
                            "                            <input type=\"submit\" class=\"btn btn-info\" id=\"show-button\" value=" + studentsOnTraining + " >\n" +
                            "                        </form>\n" +
                            "                    </td>\n";
                    break;
            }
            pageContext.getOut().write(code);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }

        return SKIP_BODY;
    }
}
