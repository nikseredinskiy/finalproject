package by.training.task.dao.impl;

import by.training.task.dao.StudentDAO;
import by.training.task.pool.ConnectionPool;
import by.training.task.entity.Student;
import by.training.task.exceptions.DAOException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;

/**
 * Created by Nik on 02.11.2015.
 */
public class StudentDAOImpl implements StudentDAO {

    private static StudentDAO instance = new StudentDAOImpl();

    private static Logger logger = Logger.getLogger(StudentDAOImpl.class);

    private static final String PUT = "INSERT INTO students (student_name, student_secondName, student_email, student_password," +
            "student_university, student_course) VALUES (?, ?, ?, ?, ?, ?)";
    private static final String PUT_TO_LOGIN = "INSERT INTO login_password (login, password, status) VALUES (?, ?, ?)";
    private static final String PUT_TO_ARCHIVE = "INSERT INTO archive (student_name, student_secondName, student_email, " +
            "student_university) VALUES (?, ?, ?, ?)";

    private static final String GET = "SELECT * FROM students";
    private static final String GET_ARCHIVE = "SELECT * FROM archive";
    private static final String GET_BY_ID = "SELECT * FROM students WHERE student_id=?";
    private static final String GET_BY_MAIL = "SELECT * FROM students WHERE student_email=?";
    private static final String GET_BY_TRAINING_ID = "SELECT * FROM training_student WHERE training_id=?";

    private static final String LOGIN = "SELECT * FROM login_password WHERE login=? AND password=?";
    private static final String UPDATE = "UPDATE students SET student_name=?, student_secondName=?, student_email=?, " +
            "student_password=?, student_university=?, student_course=? WHERE student_id=?";

    private static final String DELETE = "DELETE FROM students WHERE student_id=?";
    private static final String DELETE_LOGIN = "DELETE FROM login_password WHERE id=?";
    private static final String DELETE_FROM_TRAINING = "DELETE FROM training_student WHERE training_id=? AND student_id=?";

    private static final String REGISTER_TO_TRAINING = "INSERT INTO training_student (training_id, student_id) VALUES (?, ?)";
    private static final String IS_REGISTERED = "SELECT * FROM training_student WHERE training_id=? AND student_id=?";

    private StudentDAOImpl() {

    }

    public static StudentDAO getInstance() {
        return instance;
    }


    @Override
    public Student getById(long studentId) throws DAOException {
        Student temp = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_ID)) {
            preparedStatement.setLong(1, studentId);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String name = rs.getString("student_name");
                String secondName = rs.getString("student_secondName");
                String email = rs.getString("student_email");
                String password = rs.getString("student_password");
                String university = rs.getString("student_university");
                int course = rs.getInt("student_course");

                temp = new Student(studentId, name, secondName, email, password, university, course);
            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.getById() " + e);
        }

        return temp;
    }

    @Override
    public Student getByEmail(String mail) throws DAOException {
        Student temp = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_MAIL)) {
            preparedStatement.setString(1, mail);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                Long id = rs.getLong("student_id");
                String name = rs.getString("student_name");
                String secondName = rs.getString("student_secondName");
                String password = rs.getString("student_password");
                String university = rs.getString("student_university");
                int course = rs.getInt("student_course");

                temp = new Student(id, name, secondName, mail, password, university, course);
            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.getByEmail() " + e);
        }

        return temp;
    }

    @Override
    public boolean save(Student student) throws DAOException {
        boolean flag = false;
        String name = student.getName();
        String secondName = student.getSecondName();
        String email = student.getEmail();
        String password = student.getPassword();
        String university = student.getUniversity();
        int course = student.getCourse();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(PUT);
                PreparedStatement preparedStatement2 = conn.prepareStatement(PUT_TO_LOGIN)) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, secondName);
            preparedStatement.setString(3, email);
            preparedStatement.setString(4, password);
            preparedStatement.setString(5, university);
            preparedStatement.setInt(6, course);
            preparedStatement.execute();

            preparedStatement2.setString(1, email);
            preparedStatement2.setString(2, password);
            preparedStatement2.setString(3, "STUDENT");
            preparedStatement2.execute();

            flag = true;
        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.save() " + e);
        }
        return flag;
    }

    @Override
    public boolean update(Student student) throws DAOException {
        boolean flag = false;
        String name = student.getName();
        String secondName = student.getSecondName();
        String email = student.getEmail();
        String password = student.getPassword();
        String university = student.getUniversity();
        int course = student.getCourse();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, secondName);
            preparedStatement.setString(3, email);
            preparedStatement.setString(4, password);
            preparedStatement.setString(5, university);
            preparedStatement.setInt(6, course);
            preparedStatement.setLong(7, student.getId());
            preparedStatement.execute();
            flag = true;
        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.update() " + e);
        }
        return flag;
    }

    @Override
    public boolean delete(Long studentId) throws DAOException {
        boolean isRemoved = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(DELETE);
                PreparedStatement preparedStatement1 = conn.prepareStatement(DELETE_LOGIN);
                    PreparedStatement preparedStatement2 = conn.prepareStatement(PUT_TO_ARCHIVE)) {
            Student student = getById(studentId);
            preparedStatement.setLong(1, studentId);

            preparedStatement1.setLong(1, studentId);

            preparedStatement2.setString(1, student.getName());
            preparedStatement2.setString(2, student.getSecondName());
            preparedStatement2.setString(3, student.getEmail());
            preparedStatement2.setString(4, student.getUniversity());

            preparedStatement.execute();
            preparedStatement1.execute();
            preparedStatement2.execute();
            isRemoved = true;
        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.delete() " + e);
        }
        return isRemoved;
    }

    @Override
    public boolean registerToTraining(Long studentId, Long trainingId) throws DAOException {
        boolean flag = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(REGISTER_TO_TRAINING);
                PreparedStatement preparedStatement1 = conn.prepareStatement(IS_REGISTERED)) {
            preparedStatement.setLong(1, trainingId);
            preparedStatement.setLong(2, studentId);

            preparedStatement1.setLong(1, trainingId);
            preparedStatement1.setLong(2, studentId);

            ResultSet rs = preparedStatement1.executeQuery();
            if(!rs.next()){
                preparedStatement.execute();
            } else {
                logger.debug("You already registered");
            }

            flag = true;
        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.registerOnTraining() " + e);
        }
        return flag;
    }

    @Override
    public boolean deleteFromTraining(Long studentId, Long trainingId) throws DAOException {
        boolean isDelete = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(DELETE_FROM_TRAINING)){
            preparedStatement.setLong(1, trainingId);
            preparedStatement.setLong(2, studentId);

            preparedStatement.execute();
            isDelete = true;
        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.deleteOnTraining() " + e);
        }

        return isDelete;
    }

    @Override
    public String checkLogin(String enterLogin, String enterPass) throws DAOException {
        String role = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(LOGIN)) {
            preparedStatement.setString(1, enterLogin);
            preparedStatement.setString(2, enterPass);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                role = resultSet.getString("status");
            }
        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.checkLogin() " + e);
        }
        return role;
    }

    @Override
    public Collection<Student> getAll() throws DAOException {
        List<Student> db = new ArrayList<Student>();

        try (Connection conn = ConnectionPool.getInstance().getConnection(); Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery(GET);
            while (rs.next()) {
                long id = rs.getLong("student_id");
                String name = rs.getString("student_name");
                String secondName = rs.getString("student_secondName");
                String email = rs.getString("student_email");
                String password = rs.getString("student_password");
                String university = rs.getString("student_university");
                int course = rs.getInt("student_course");

                Student temp = new Student(id, name, secondName, email, password, university, course);
                db.add(temp);

            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.getAll() " + e);
        }
        return db;
    }

    @Override
    public Collection<Student> getOnTraining(Long trainingId) throws DAOException {
        List<Student> db = new ArrayList<Student>();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_TRAINING_ID)) {
            preparedStatement.setLong(1, trainingId);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                long id = rs.getLong("student_id");

                Student temp = getById(id);
                db.add(temp);
            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.getOnTraining() " + e);
        }
        return db;
    }

    @Override
    public Collection<Student> getArchive() throws DAOException {
        List<Student> db = new ArrayList<Student>();

        try (Connection conn = ConnectionPool.getInstance().getConnection(); Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery(GET_ARCHIVE);
            while (rs.next()) {
                String name = rs.getString("student_name");
                String secondName = rs.getString("student_secondName");
                String email = rs.getString("student_email");
                String university = rs.getString("student_university");

                Student temp = new Student(name, secondName, email, university);
                db.add(temp);

            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.getAll() " + e);
        }
        return db;
    }

    @Override
    public boolean isOnTraining(Long studentId, Long trainingId) throws DAOException {
        boolean isOnTraining = false;

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(IS_REGISTERED)) {
            preparedStatement.setLong(1, trainingId);
            preparedStatement.setLong(2, studentId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                isOnTraining = true;
            }
        } catch (SQLException e) {
            throw new DAOException("Error while StudentDAO.checkLogin() " + e);
        }
        return isOnTraining;
    }
}