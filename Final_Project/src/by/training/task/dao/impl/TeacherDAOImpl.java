package by.training.task.dao.impl;

import by.training.task.dao.TeacherDAO;
import by.training.task.pool.ConnectionPool;
import by.training.task.entity.Teacher;
import by.training.task.exceptions.DAOException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;

/**
 * Created by Nik on 11.11.2015.
 */
public class TeacherDAOImpl implements TeacherDAO {

    private static TeacherDAO instance = new TeacherDAOImpl();

    private static Logger logger = Logger.getLogger(TeacherDAOImpl.class);

    private static final String PUT = "INSERT INTO teachers (teacher_name, teacher_secondName, teacher_email, teacher_password)" +
            "VALUES (?, ?, ?, ?)";
    private static final String PUT_TO_LOGIN = "INSERT INTO login_password (id, login, password, status) VALUES (?, ?, ?, ?)";
    private static final String GET = "SELECT * FROM teachers";
    private static final String GET_BY_ID = "SELECT * FROM teachers WHERE teacher_id=";
    private static final String GET_BY_EMAIL = "SELECT * FROM teachers WHERE teacher_email=?";
    private static final String UPDATE = "UPDATE teachers SET teacher_name=?, teacher_secondName=?, teacher_email=?, " +
            "teacher_password=? WHERE teacher_id=?";
    private static final String DELETE = "DELETE FROM teachers WHERE teacher_id=?";

    private TeacherDAOImpl() {

    }

    public static TeacherDAO getInstance() {
        return instance;
    }

    @Override
    public Teacher getById(Long teacherId) throws DAOException {
        Teacher temp = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection(); Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery(GET_BY_ID + teacherId);

            while (rs.next()) {
                String name = rs.getString("teacher_name");
                String secondName = rs.getString("teacher_secondName");
                String email = rs.getString("teacher_email");
                String password = rs.getString("teacher_password");

                temp = new Teacher(teacherId, name, secondName, email, password);
            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while TeacherDAO.getById() " + e);
        }

        return temp;
    }

    @Override
    public boolean save(Teacher teacher) throws DAOException {
        boolean flag = false;
        String name = teacher.getName();
        String secondName = teacher.getSecondName();
        String email = teacher.getEmail();
        String password = teacher.getPassword();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(PUT);
                PreparedStatement preparedStatement2 = conn.prepareStatement(PUT_TO_LOGIN);
                    PreparedStatement preparedStatement3 = conn.prepareStatement(GET_BY_EMAIL)) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, secondName);
            preparedStatement.setString(3, email);
            preparedStatement.setString(4, password);
            preparedStatement.execute();

            preparedStatement3.setString(1, email);
            ResultSet resultSet = preparedStatement3.executeQuery();
            long id = 0;
            while (resultSet.next()){
                id = resultSet.getLong("teacher_id");
            }

            preparedStatement2.setLong(1, id);
            preparedStatement2.setString(2, email);
            preparedStatement2.setString(3, password);
            preparedStatement2.setString(4, "TEACHER");
            preparedStatement2.execute();

            flag = true;
        } catch (SQLException e) {
            throw new DAOException("Error while TeacherDAO.save() " + e);
        }
        return flag;
    }

    @Override
    public boolean update(Teacher teacher) throws DAOException {
        boolean flag = false;
        String name = teacher.getName();
        String secondName = teacher.getSecondName();
        String email = teacher.getEmail();
        String password = teacher.getPassword();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, secondName);
            preparedStatement.setString(3, email);
            preparedStatement.setString(4, password);
            preparedStatement.setLong(5, teacher.getId());
            preparedStatement.execute();
            flag = true;
        } catch (SQLException e) {
            throw new DAOException("Error while TeacherDAO.update() " + e);
        }
        return flag;
    }

    @Override
    public boolean delete(Long teacherId) throws DAOException {
        boolean isRemoved = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(DELETE)) {
            preparedStatement.setLong(1, teacherId);
            preparedStatement.execute();
            isRemoved = true;
        } catch (SQLException e) {
            throw new DAOException("Error while TeacherDAO.delete() " + e);
        }
        return isRemoved;
    }

    @Override
    public Collection<Teacher> getAll() throws DAOException {
        List<Teacher> db = new ArrayList<Teacher>();

        try (Connection conn = ConnectionPool.getInstance().getConnection(); Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery(GET);
            while (rs.next()) {
                long id = rs.getLong("teacher_id");
                String name = rs.getString("teacher_name");
                String secondName = rs.getString("teacher_secondName");
                String email = rs.getString("teacher_email");
                String password = rs.getString("teacher_password");

                Teacher temp = new Teacher(id, name, secondName, email, password);
                db.add(temp);

            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while TeacherDAO.getAll() " + e);
        }
        return db;
    }
}
