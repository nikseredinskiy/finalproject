package by.training.task.dao.impl;

import by.training.task.dao.TrainingDAO;
import by.training.task.pool.ConnectionPool;
import by.training.task.entity.Teacher;
import by.training.task.entity.Training;
import by.training.task.exceptions.DAOException;
import by.training.task.exceptions.ServiceException;
import by.training.task.service.TeacherService;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by Nik on 09.11.2015.
 */
public class TrainingDAOImpl implements TrainingDAO {
    private static TrainingDAO instance = new TrainingDAOImpl();

    TeacherService teacherService = TeacherService.getInstance();

    private static Logger logger = Logger.getLogger(TrainingDAOImpl.class);

    private static final String PUT = "INSERT INTO trainings (training_name, training_startDate, training_endDate, training_teacherId, training_status)" +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String GET = "SELECT * FROM trainings";
    private static final String GET_BY_ID = "SELECT * FROM trainings WHERE training_id=";
    private static final String FINISH_TRAINING = "UPDATE trainings SET training_status=\'FINISHED\' WHERE training_id=?";
    private static final String CHECK_FOR_FINISHED = "UPDATE trainings SET training_status='FINISHED'" +
            " WHERE training_endDate < CURDATE()";


    private TrainingDAOImpl(){

    }

    public static TrainingDAO getInstance() {
        return instance;
    }


    @Override
    public Training getByTrainingId(Long trainingId) throws DAOException {
        Training temp = null;
        try (Connection conn = ConnectionPool.getInstance().getConnection(); Statement statement = conn.createStatement()) {
            ResultSet rs = statement.executeQuery(GET_BY_ID + trainingId);

            while (rs.next()) {
                String name = rs.getString("training_name");
                Date startDate = rs.getDate("training_startDate");
                Date endDate = rs.getDate("training_endDate");
                Long teacherId = rs.getLong("training_teacherId");
                Teacher teacher = new Teacher();
                String status = rs.getString("training_status");

                try {
                    teacher = teacherService.getById(teacherId);
                }catch (ServiceException e){

                }

                temp = new Training(trainingId, name, startDate, endDate, teacher, status);
            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while TrainingDAO.getByTrainingId() " + e);
        }

        return temp;
    }

    @Override
    public boolean save(Training training) throws DAOException {
        boolean flag = false;
        String name = training.getName();
        Date startDate = training.getStartDate();
        Date endDate = training.getEndDate();
        Long teacherId = training.getTeacher().getId();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(PUT)) {
            preparedStatement.setString(1, name);
            preparedStatement.setDate(2, new java.sql.Date(startDate.getTime()));
            preparedStatement.setDate(3, new java.sql.Date(endDate.getTime()));
            preparedStatement.setLong(4, teacherId);
            preparedStatement.setString(5, "STARTED");
            preparedStatement.execute();

            flag = true;
        } catch (SQLException e) {
            throw new DAOException("Error while TrainingDAO.save() " + e);
        }
        return flag;
    }

    @Override
    public Collection<Training> getAll() throws DAOException {
        List<Training> db = new ArrayList<Training>();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             Statement statement = conn.createStatement()) {

            checkForFinished();

            ResultSet rs = statement.executeQuery(GET);
            while (rs.next()) {
                long id = rs.getLong("training_id");
                String name = rs.getString("training_name");
                Date startDate = rs.getDate("training_startDate");
                Date endDate = rs.getDate("training_endDate");
                long teacherId = rs.getLong("training_teacherId");
                String status = rs.getString("training_status");
                Teacher teacher = new Teacher();
                try{
                    teacher = teacherService.getById(teacherId);
                }catch (ServiceException e){

                }

                if("STARTED".equals(status)){
                    Training temp = new Training(id, name, startDate, endDate, teacher, status);
                    db.add(temp);
                }

            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while TeacherDAO.getAll() " + e);
        }
        return db;
    }

    @Override
    public Collection<Training> getAllFinished() throws DAOException {
        List<Training> db = new ArrayList<Training>();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             Statement statement = conn.createStatement()) {

            checkForFinished();

            ResultSet rs = statement.executeQuery(GET);
            while (rs.next()) {
                long id = rs.getLong("training_id");
                String name = rs.getString("training_name");
                Date startDate = rs.getDate("training_startDate");
                Date endDate = rs.getDate("training_endDate");
                long teacherId = rs.getLong("training_teacherId");
                String status = rs.getString("training_status");
                Teacher teacher = new Teacher();
                try{
                    teacher = teacherService.getById(teacherId);
                }catch (ServiceException e){
                    throw new DAOException("Error while TeacherDAO.getAllFinished() " + e);
                }

                if("FINISHED".equals(status)){
                    Training temp = new Training(id, name, startDate, endDate, teacher, status);
                    db.add(temp);
                }

            }
            rs.close();

        } catch (SQLException e) {
            throw new DAOException("Error while TeacherDAO.getAllFinished() " + e);
        }
        return db;
    }

    @Override
    public boolean finishTraining(Long trainingId) throws DAOException {
        boolean flag = false;
        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(FINISH_TRAINING)){
            preparedStatement.setLong(1, trainingId);

            preparedStatement.execute();
            flag = true;
        } catch (SQLException e) {
            throw new DAOException("Error while TrainingDAO.finishTraining() " + e);
        }
        return flag;
    }

    private static boolean checkForFinished() throws DAOException{
        boolean flag = false;
        try(Connection conn = ConnectionPool.getInstance().getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement(CHECK_FOR_FINISHED)){

            preparedStatement.execute();
            flag = true;
        } catch (SQLException e){
            throw new DAOException("Error while TrainingDAO.finishTraining() " + e);
        }

        return flag;
    }
}
