package by.training.task.dao.impl;

import by.training.task.dao.FeedbackDAO;
import by.training.task.pool.ConnectionPool;
import by.training.task.entity.Feedback;
import by.training.task.exceptions.DAOException;
import by.training.task.exceptions.ServiceException;
import by.training.task.service.TrainingService;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Nik on 28.11.2015.
 */
public class FeedbackDAOImpl implements FeedbackDAO {

    private static FeedbackDAO instance = new FeedbackDAOImpl();

    private static Logger logger = Logger.getLogger(FeedbackDAOImpl.class);

    private static final String PUT = "INSERT INTO feedbacks (feedback_studentId, feedback_trainingId, feedback_text, feedback_mark)" +
            " VALUES (?, ?, ?, ?)";
    private static final String GET_BY_STUDENT_ID = "SELECT * FROM feedbacks WHERE feedback_studentId=?";

    private FeedbackDAOImpl() {

    }

    public static FeedbackDAO getInstance() {
        return instance;
    }

    @Override
    public Feedback getById(long paramLong) {
        return null;
    }

    @Override
    public Feedback getByStudentId(long studentId) {
        return null;
    }

    @Override
    public boolean save(Feedback feedback) throws DAOException {
        boolean flag = false;
        Long studentId = feedback.getStudentId();
        Long trainingId = feedback.getTrainingId();
        String text = feedback.getFeedback();
        int mark = feedback.getMark();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(PUT)) {
            preparedStatement.setLong(1, studentId);
            preparedStatement.setLong(2, trainingId);
            preparedStatement.setString(3, text);
            preparedStatement.setInt(4, mark);
            preparedStatement.execute();

            flag = true;
        } catch (SQLException e) {
            throw new DAOException("Error while FeedbackDAO.save() " + e);
        }
        return flag;
    }

    @Override
    public boolean update(Feedback feedback) {
        return false;
    }

    @Override
    public Collection<Feedback> getAllByStudentId(long studentId) throws DAOException {
        List<Feedback> db = new ArrayList<Feedback>();

        try (Connection conn = ConnectionPool.getInstance().getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(GET_BY_STUDENT_ID)) {
            preparedStatement.setLong(1,studentId);
            ResultSet rs = preparedStatement.executeQuery();

            TrainingService trainingService = TrainingService.getInstance();

            while (rs.next()) {
                long id = rs.getLong("feedback_id");
                long student_Id = rs.getLong("feedback_studentId");
                long training_Id = rs.getLong("feedback_trainingId");
                String text = rs.getString("feedback_text");
                int mark = rs.getInt("feedback_mark");

                Feedback temp = new Feedback(id, student_Id, training_Id, text, mark);


                try{
                    temp.setTrainingName(trainingService.getById(training_Id).getName());
                }catch (ServiceException e){
                    throw new DAOException("Error while FeedbackDAO().getAllByStudentId() " + e);
                }

                db.add(temp);
            }
            rs.close();

        } catch (SQLException e) {
                throw new DAOException("Error while FeedbackDAO.getAllByStudentID() " + e);
        }
        return db;
    }
}
