package by.training.task.dao;

import by.training.task.entity.Training;
import by.training.task.exceptions.DAOException;

import java.util.Collection;

/**
 * Created by Nik on 09.11.2015.
 */
public interface TrainingDAO {
    Training getByTrainingId(Long paramLong) throws DAOException;

    boolean save(Training training) throws DAOException;

    Collection<Training> getAll() throws DAOException;

    Collection<Training> getAllFinished() throws DAOException;

    boolean finishTraining(Long trainingId) throws DAOException;
}
