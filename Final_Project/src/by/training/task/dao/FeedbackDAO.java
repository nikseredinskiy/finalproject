package by.training.task.dao;

import by.training.task.entity.Feedback;
import by.training.task.exceptions.DAOException;

import java.util.Collection;

/**
 * Created by Nik on 28.11.2015.
 */
public interface FeedbackDAO {
     Feedback getById(long paramLong);

    Feedback getByStudentId(long paramLong);

    Collection<Feedback> getAllByStudentId(long paramLong) throws DAOException;

    boolean save(Feedback feedback) throws DAOException;

    boolean update(Feedback feedback);
}
