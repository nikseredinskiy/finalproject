package by.training.task.dao;

import by.training.task.entity.Student;
import by.training.task.exceptions.DAOException;

import java.util.Collection;

/**
 * Created by Nik on 02.11.2015.
 */
public interface StudentDAO {
    Student getById(long paramLong) throws DAOException;

    Student getByEmail(String mail) throws DAOException;

    boolean save(Student student) throws DAOException;

    boolean update(Student student) throws DAOException;

    String checkLogin(String login, String pass) throws DAOException;

    boolean delete(Long paramLong) throws DAOException;

    boolean registerToTraining(Long studentId, Long trainingId) throws DAOException;

    boolean deleteFromTraining(Long studentId, Long trainingId) throws DAOException;

    Collection<Student> getAll() throws DAOException;

    Collection<Student> getArchive() throws DAOException;

    Collection<Student> getOnTraining(Long trainingId) throws DAOException;

    boolean isOnTraining(Long studentId, Long trainingId) throws DAOException;
}
