package by.training.task.dao;

import by.training.task.entity.Teacher;
import by.training.task.exceptions.DAOException;

import java.util.Collection;

/**
 * Created by Nik on 11.11.2015.
 */
public interface TeacherDAO {
    Teacher getById(Long paramLong) throws DAOException;

    boolean save(Teacher teacher) throws DAOException;

    boolean update(Teacher teacher) throws DAOException;

    boolean delete(Long paramLong) throws DAOException;

    Collection<Teacher> getAll() throws DAOException;
}
