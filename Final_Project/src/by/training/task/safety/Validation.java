package by.training.task.safety;

import by.training.task.entity.Student;
import by.training.task.entity.Teacher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nik on 07.12.2015.
 */
public class Validation {

    private final String domen = "[a-z][a-z[0-9]\u005F\u002E\u002D]*[a-z||0-9]";
    private final String prefix = "([a-z]){2,4}";
    private final String REGEX_FOR_NAME = "[a-zA-Z ]{3,30}";
    private final String REGEX_FOR_EMAIL = domen + "@" + domen + "\u002E" + prefix;

    public boolean validate(Student student){
        boolean flag = false;
        if(validateData(student.getName(), REGEX_FOR_NAME) && validateData(student.getSecondName(), REGEX_FOR_NAME)
                && validateData(student.getEmail(), REGEX_FOR_EMAIL)){
            flag = true;
        }

        return flag;
    }

    public boolean validate(Teacher teacher){
        boolean flag = false;
        if(validateData(teacher.getName(), REGEX_FOR_NAME) && validateData(teacher.getSecondName(), REGEX_FOR_NAME)
                && validateData(teacher.getEmail(), REGEX_FOR_EMAIL)){
            flag = true;
        }

        return flag;
    }

    private boolean validateData(String name, String regex){
        boolean flag = false;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(name);

        if(matcher.matches()){
            flag = true;
        }

        return flag;
    }
}
