package by.training.task.safety;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Nik on 24.11.2015.
 */
public class Encryption {
    public static String codePassword(String password) {
        MessageDigest md5;
        StringBuffer codeResult = new StringBuffer();

        try {
            md5 = MessageDigest.getInstance("md5");
            md5.update(password.getBytes());
            byte messageDigest[] = md5.digest();
            for (int i = 0; i < messageDigest.length; i++) {
                if ((0xff & messageDigest[i]) < 0x10) {
                    codeResult.append("0" + Integer.toHexString((0xFF & messageDigest[i])));
                }
                else {
                    codeResult.append(Integer.toHexString(0xFF & messageDigest[i]));
                }
            }
        } catch (NoSuchAlgorithmException ex) {
        }
        return codeResult.toString();
    }
}
