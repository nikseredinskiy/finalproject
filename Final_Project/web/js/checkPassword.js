/**
 * Created by Nik on 03.12.2015.
 */
function checkPass()
{
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');

    var submit = document.getElementById('update-submit');

    var goodColor = "#C7FFD8";
    var badColor = "#FFC9C7";

    if(pass1.value == ''){
        pass2.style.background = '';
        pass2.value = '';
        submit.disabled = false;
    }else{
        if(pass1.value == pass2.value){
            pass2.style.backgroundColor = goodColor;
            submit.disabled = false;
        }else{
            pass2.style.backgroundColor = badColor;
            submit.disabled = true;
        }
    }
}
