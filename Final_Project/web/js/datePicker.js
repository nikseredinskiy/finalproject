/**
 * Created by Nik on 21.12.2015.
 */

function pickToday(){
    var date = new Date();

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if(month < 10){
        month = "0" + month;
    }

    if(day < 10){
        day = "0" + day;
    }

    var today = year + "-" + month + "-" + day;
    var endDate = document.getElementById("end-date").value;
    document.getElementById("start-date").min = today;
    document.getElementById("start-date").max = endDate;

    var startDate = document.getElementById("start-date").value;
    document.getElementById("end-date").min = startDate;
}