<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 09.11.2015
  Time: 7:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="label.lists-title" bundle="${rb}"/></title>
    <link rel="shortcut icon" type="image/x-icon" href="../img/icon3.png"/>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="../js/tabHistory.js"></script>
</head>
<body>
<c:import url="header.jsp"/>
<div class="container">
    <c:choose>
        <c:when test="${role == 'GUEST'}">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#trainings-list1"><fmt:message key="label.trainings-list"
                                                                                             bundle="${rb}"/></a></li>
            </ul>
            <div class="tab-content">
                <div id="trainings-list1" class="tab-pane fade in active">
                    <h3><fmt:message key="label.trainings-list" bundle="${rb}"/></h3>
                    <c:import url="trainings-list.jsp"/>
                </div>
            </div>
        </c:when>

        <c:when test="${role == 'STUDENT'}">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#trainings-list2"><fmt:message key="label.trainings-list"
                                                                                             bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#feedbacks-list2"><fmt:message key="label.feedbacks"
                                                                              bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#teachers-list2"><fmt:message key="label.teachers-list"
                                                                             bundle="${rb}"/></a></li>
            </ul>
            <div class="tab-content">
                <div id="trainings-list2" class="tab-pane fade in active">
                    <h3><fmt:message key="label.trainings-list" bundle="${rb}"/></h3>
                    <c:import url="trainings-list.jsp"/>
                </div>
                <div id="feedbacks-list2" class="tab-pane fade">
                    <h3><fmt:message key="label.feedbacks" bundle="${rb}"/></h3>
                    <c:import url="feedbacks-list.jsp"/>
                </div>
                <div id="teachers-list2" class="tab-pane fade">
                    <h3><fmt:message key="label.teachers-list" bundle="${rb}"/></h3>
                    <c:import url="teachers-list.jsp"/>
                </div>
            </div>
        </c:when>

        <c:when test="${role == 'TEACHER'}">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#students-list3"><fmt:message key="label.students-list"
                                                                                            bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#trainings-list3"><fmt:message key="label.trainings-list"
                                                                              bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#finished-trainings-list3"><fmt:message
                        key="label.finished-trainings-list"
                        bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#teachers-list3"><fmt:message key="label.teachers-list"
                                                                             bundle="${rb}"/></a></li>
            </ul>

            <div class="tab-content">
                <div id="students-list3" class="tab-pane fade in active">
                    <h3><fmt:message key="label.students-list" bundle="${rb}"/></h3>
                    <c:import url="students-list.jsp"/>
                </div>
                <div id="trainings-list3" class="tab-pane fade">
                    <h3><fmt:message key="label.trainings-list" bundle="${rb}"/></h3>
                    <c:import url="trainings-list.jsp"/>
                </div>
                <div id="finished-trainings-list3" class="tab-pane fade">
                    <h3><fmt:message key="label.finished-trainings-list" bundle="${rb}"/></h3>
                    <c:import url="finished-trainings-list.jsp"/>
                </div>
                <div id="teachers-list3" class="tab-pane fade">
                    <h3><fmt:message key="label.teachers-list" bundle="${rb}"/></h3>
                    <c:import url="teachers-list.jsp"/>
                </div>
            </div>
        </c:when>

        <c:when test="${role == 'ADMINISTRATOR'}">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#students-list"><fmt:message key="label.students-list"
                                                                                           bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#trainings-list"><fmt:message key="label.trainings-list"
                                                                             bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#finished-trainings-list"><fmt:message
                        key="label.finished-trainings-list"
                        bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#teachers-list"><fmt:message key="label.teachers-list"
                                                                            bundle="${rb}"/></a></li>
                <li><a data-toggle="tab" href="#archive-list"><fmt:message key="label.archive-list" bundle="${rb}"/></a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="students-list" class="tab-pane fade in active">
                    <h3><fmt:message key="label.students-list" bundle="${rb}"/></h3>
                    <c:import url="students-list.jsp"/>
                </div>
                <div id="trainings-list" class="tab-pane fade">
                    <h3><fmt:message key="label.trainings-list" bundle="${rb}"/></h3>
                    <c:import url="trainings-list.jsp"/>
                </div>
                <div id="finished-trainings-list" class="tab-pane fade">
                    <h3><fmt:message key="label.finished-trainings-list" bundle="${rb}"/></h3>
                    <c:import url="finished-trainings-list.jsp"/>
                </div>
                <div id="teachers-list" class="tab-pane fade">
                    <h3><fmt:message key="label.teachers-list" bundle="${rb}"/></h3>
                    <c:import url="teachers-list.jsp"/>
                </div>
                <div id="archive-list" class="tab-pane fade">
                    <h3><fmt:message key="label.archive-list" bundle="${rb}"/></h3>
                    <c:import url="archive-list.jsp"/>
                </div>
            </div>
        </c:when>
    </c:choose>
</div>
<c:import url="footer.jsp" />
</body>
</html>
