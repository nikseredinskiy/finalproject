<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 23.11.2015
  Time: 8:37
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><fmt:message key="label.students-on-training" bundle="${rb}"/></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<c:import url="header.jsp"/>
<div class="container">
    <hr>
    <div id="add-student">
        <c:if test="${not empty studentsOnTraining}">
            <table id="newspaper-b">
                <tr>
                    <th>ID</th>
                    <th><fmt:message key="label.name" bundle="${rb}"/></th>
                    <th><fmt:message key="label.secondName" bundle="${rb}"/></th>
                    <th><fmt:message key="label.email" bundle="${rb}"/></th>
                    <th><fmt:message key="label.university" bundle="${rb}"/></th>
                    <th><fmt:message key="label.course" bundle="${rb}"/></th>
                    <th></th>
                </tr>
                <c:forEach items="${studentsOnTraining}" var="student">
                    <tr>
                        <td><c:out value="${student.id}"/></td>
                        <td><c:out value="${student.name}"/></td>
                        <td><c:out value="${student.secondName}"/></td>
                        <td><c:out value="${student.email}"/></td>
                        <td><c:out value="${student.university}"/></td>
                        <td><c:out value="${student.course}"/></td>
                        <td>
                            <c:if test="${training.status == 'FINISHED'}">
                                <form action="/controller" id="feedbackForm" method="post">
                                    <input type="hidden" name="command" value="NEW_FEEDBACK">
                                    <input type="hidden" id="feedbackStudentId" name="feedbackStudentId">
                                    <input type="hidden" name="training_Id" value="${training.id}">
                                    <input type="submit" class="btn btn-info" id="feedback-button" value=
                                        <fmt:message key="label.add-feedback" bundle="${rb}"/> form="feedbackForm"
                                           onclick='document.getElementById("feedbackStudentId").value="${student.id}"'>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        <form action="/controller" method="post" id="back-form">
            <input type="hidden" name="command" value="back">
        </form>
        <input type="submit" form="back-form" style="float: right;" class="btn btn-danger"
               value=<fmt:message key="label.back" bundle="${rb}"/>>
    </div>
</div>
<c:import url="footer.jsp" />
</body>
</html>
