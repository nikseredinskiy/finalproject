<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 28.11.2015
  Time: 10:15
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><fmt:message key="label.add-feedback" bundle="${rb}"/></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<c:import url="header.jsp"/>
<div class="container">
    <div id="login-form">
        <h3><fmt:message key="label.feedback" bundle="${rb}"/></h3>
        <fieldset>
            <form action="/controller" method="post" id="feedback-form">
                <input type="hidden" name="command" value="add-feedback">
                <input type="hidden" name="stId" value="${stId}">
                <input type="hidden" name="trId" value="${trId}">
                <div class="control-label"><fmt:message key="label.feedback" bundle="${rb}"/>:</div>
                <textarea name="feedbackText" required placeholder="Text feedback here..."></textarea>
                <br />
                <div class="control-label"><fmt:message key="label.mark" bundle="${rb}"/>:</div>
                <select name="feedbackMark" required>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </form>
            <form action="/controller" method="post" id="back-form">
                <input type="hidden" name="command" value="back">
            </form>
            <input type="submit" form="back-form" style="float: none;" class="btn btn-danger"
                   value=<fmt:message key="label.back" bundle="${rb}"/>>
            <input type="submit" form="feedback-form" class="btn btn-default"
                   value=<fmt:message key="label.add-feedback" bundle="${rb}"/>>
        </fieldset>
    </div>
</div>
<c:import url="footer.jsp" />
</body>
</html>
