<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 09.11.2015
  Time: 8:02
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><fmt:message key="label.add-training" bundle="${rb}"/></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="../js/datePicker.js"></script>

    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<c:import url="header.jsp"/>
<div class="container">
    <div id="login-form">
        <h3><fmt:message key="label.create-training" bundle="${rb}"/></h3>
        <fieldset>
            <form action="/controller" method="post" id="training-form">
                <input type="hidden" name="command" value="add-training">

                <div class="control-label"><fmt:message key="label.name" bundle="${rb}"/>:</div>
                <input type="text" name="name" required>
                <br/>

                <div class="control-label"><fmt:message key="label.start-date" bundle="${rb}"/>:</div>
                <br/>
                <input type="date" id="start-date" onchange="pickToday()" name="startDate" required>
                <br/>

                <div class="control-label"><fmt:message key="label.end-date" bundle="${rb}"/>:</div>
                <br/>
                <input type="date" id="end-date" onchange="pickToday()" name="endDate" required>
                <br/>

                <div class="control-label"><fmt:message key="label.teacher" bundle="${rb}"/>:</div>
                <br/>
                <c:if test="${not empty teachers}">
                    <select name="teacherId">
                        <c:forEach items="${teachers}" var="teacher">
                            <option value="${teacher.id}">${teacher.name} ${teacher.secondName}</option>
                        </c:forEach>
                    </select>
                </c:if>
            </form>
            <form action="/controller" method="post" id="back-form">
                <input type="hidden" name="command" value="back">
            </form>
            <input type="submit" form="back-form" style="float: none;" class="btn btn-danger"
                   value=<fmt:message key="label.back" bundle="${rb}"/>>
            <input type="submit" form="training-form" class="btn btn-default" value=<fmt:message key="label.create-training"
                                                                            bundle="${rb}"/>>
        </fieldset>
    </div>
</div>
</body>
<script>pickToday()</script>
<c:import url="footer.jsp" />
</html>
