<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 09.11.2015
  Time: 8:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="label.trainings-list" bundle="${rb}" /></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="container">
    <div id="add-student">
        <form id="createTrainingForm" action="/controller" method="post">
            <input type="hidden" name="command" value="new-training">
        </form>
        <c:if test="${not empty trainings}">
            <table id="newspaper-b">
                <tr>
                    <th><fmt:message key="label.name" bundle="${rb}" /></th>
                    <th><fmt:message key="label.teacher" bundle="${rb}" /></th>
                    <th><fmt:message key="label.start-date" bundle="${rb}" /></th>
                    <th><fmt:message key="label.end-date" bundle="${rb}" /></th>
                    <th></th>
                    <th></th>
                </tr>
                <c:forEach items="${trainings}" var="training">
                    <tr>
                        <td><c:out value="${training.name}" /></td>
                        <td><c:out value="${training.teacher.name}"/> <c:out value="${training.teacher.secondName}"/></td>
                        <td><c:out value="${training.startDate}"/></td>
                        <td><c:out value="${training.endDate}"/></td>
                        <ctg:trainingListTag role="${role}" trainingID="${training.id}" />
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        <c:if test="${role == 'ADMINISTRATOR'}">
            <input type="submit" form="createTrainingForm" style="margin-bottom: 30px" class="btn btn-info" value=<fmt:message key="label.add-training" bundle="${rb}" />>
        </c:if>
    </div>
</div>

</body>
</html>
