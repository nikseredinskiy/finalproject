<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 12.12.2015
  Time: 9:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="label.finished-trainings-list" bundle="${rb}" /></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="add-student">
    <form></form>
    <c:if test="${not empty finishedTrainings}">
        <table id="newspaper-b">
            <tr>
                <th><fmt:message key="label.name" bundle="${rb}" /></th>
                <th><fmt:message key="label.teacher" bundle="${rb}" /></th>
                <th><fmt:message key="label.start-date" bundle="${rb}" /></th>
                <th><fmt:message key="label.end-date" bundle="${rb}" /></th>
                <th></th>
            </tr>
            <c:forEach items="${finishedTrainings}" var="training">
                <tr>
                    <td><c:out value="${training.name}" /></td>
                    <td><c:out value="${training.teacher.name}"/> <c:out value="${training.teacher.secondName}"/></td>
                    <td><c:out value="${training.startDate}"/></td>
                    <td><c:out value="${training.endDate}"/></td>
                    <ctg:finTrainingListTag role="${role}" trainingID="${training.id}" />
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>

</body>
</html>

