<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 01.11.2015
  Time: 11:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="css/style.css">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="label.welcome" bundle="${rb}"/></title>
</head>
<body>
<c:import url="header.jsp"/>
<div class="container">
    <div id="login-form">
        <h3><fmt:message key="label.login" bundle="${rb}"/></h3>
        <fieldset>
            <form action="/controller" method="post" id="login">
                <input type="hidden" name="command" value="login">
                <fmt:message key="label.login" bundle="${rb}"/>:
                <input type="text" name="login">
                <fmt:message key="label.password" bundle="${rb}"/>:
                <input type="password" name="password">
                ${errorLoginPassMessage}
                ${wrongAction}
                ${nullPage}
            </form>
            <form action="/controller" method="post" id="registration">
                <input type="hidden" name="command" value="new-student">
            </form>
            <input class="btn btn-default" style="float: none" type="submit" form="registration" value="<fmt:message key="label.registration" bundle="${rb}"/>">
            <input class="btn btn-info" type="submit" form="login" value="<fmt:message key="label.login" bundle="${rb}"/>">
        </fieldset>
    </div>
</div>
<c:import url="footer.jsp" />
</body>
</html>
