<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 12.12.2015
  Time: 17:53
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<div id="add-student">
    <form></form>
    <c:if test="${not empty currStudentFeedbacks}">
        <table id="newspaper-b">
            <tr>
                <th><fmt:message key="label.name" bundle="${rb}"/></th>
                <th><fmt:message key="label.feedback" bundle="${rb}"/></th>
                <th><fmt:message key="label.mark" bundle="${rb}"/></th>
            </tr>
            <c:forEach items="${currStudentFeedbacks}" var="feedback">
                <tr>
                    <td><c:out value="${feedback.trainingName}"/></td>
                    <td><c:out value="${feedback.feedback}"/></td>
                    <td><c:out value="${feedback.mark}"/></td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>
</body>
</html>
