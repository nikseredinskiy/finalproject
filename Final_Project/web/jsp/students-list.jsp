<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 02.11.2015
  Time: 9:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="label.students-list" bundle="${rb}" /></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="add-student">
    <form id="createForm" action="/controller" method="post">
        <input type="hidden" name="command" value="new-student">
    </form>
    <c:if test="${not empty students}">
        <table id="newspaper-b">
            <tr>
                <th>ID</th>
                <th><fmt:message key="label.name" bundle="${rb}" /></th>
                <th><fmt:message key="label.secondName" bundle="${rb}" /></th>
                <th><fmt:message key="label.email" bundle="${rb}" /></th>
                <th><fmt:message key="label.university" bundle="${rb}" /></th>
                <th><fmt:message key="label.course" bundle="${rb}" /></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            <c:forEach items="${students}" var="student">
                <tr>
                    <td><c:out value="${student.id}"/></td>
                    <td><c:out value="${student.name}"/></td>
                    <td><c:out value="${student.secondName}"/></td>
                    <td><c:out value="${student.email}"/></td>
                    <td><c:out value="${student.university}"/></td>
                    <td><c:out value="${student.course}"/></td>
                    <ctg:studentListTag role="${role}" studentID="${student.id}" />
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <c:if test="${role == 'ADMINISTRATOR'}">
        <input type="submit" class="btn btn-info" style="margin-bottom: 30px" value=<fmt:message key="label.add-student" bundle="${rb}" /> form="createForm">
    </c:if>
</div>
</body>
</html>
