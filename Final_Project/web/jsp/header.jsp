<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 03.12.2015
  Time: 9:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="/WEB-INF/tld/custom.tld" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <title></title>
</head>
<body>
<div class="header">
    <div class="container">
        <div class="pnl">
            <h2><fmt:message key="label.project-name" bundle="${rb}"/></h2>
        </div>
        <br />
        <br />
        <br />
        <div class="pnl">
            <c:choose>
                <c:when test="${role == 'GUEST'}">
                    <div>
                        <form action="/controller" method="post" style="margin-bottom: 0">
                            <input type="hidden" name="command" value="sign-up">
                            <input type="submit" class="btn btn-default header-btn"
                                   value="<fmt:message key="label.sign-up" bundle="${rb}" />">
                            <input type="submit" form="registration" class="btn btn-default header-btn"
                                   value="<fmt:message key="label.registration" bundle="${rb}" />">
                        </form>
                        <form action="/controller" method="post" id="registration" style="margin-bottom: 0">
                            <input type="hidden" name="command" value="new-student">
                        </form>
                    </div>
                </c:when>

                <c:when test="${role != 'GUEST'}">
                    <div>
                        <form action="/controller" method="post" style="margin-bottom: 0">
                            <input type="hidden" name="command" value="logout">
                            <input type="submit" class="btn btn-default header-btn"
                                   value="<fmt:message key="label.logout" bundle="${rb}" />">
                        </form>
                    </div>
                </c:when>
            </c:choose>
        </div>
        <div class="pnl-label">
            <fmt:message key="label.login-as" bundle="${rb}"/>: ${role}
        </div>
        <br />
        <br />
        <div class="pnl">
            <form action="/controller" method="post">
                <input type="hidden" name="command" value="change-locale">
                <input type="submit" class="btn btn-default header-btn" name="locale" value="Eng">
                <input type="submit" class="btn btn-default header-btn" name="locale" value="Rus">
            </form>
        </div>
        <div class="pnl-label">
            <fmt:message key="label.locale" bundle="${rb}"/>:
        </div>
    </div>
</div>
</body>
</html>
