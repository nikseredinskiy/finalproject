<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 11.11.2015
  Time: 10:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="label.teachers-list" bundle="${rb}"/></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="add-student">
    <form id="new-teacher" action="/controller" method="post">
        <input type="hidden" name="command" value="new-teacher">
    </form>
    <c:if test="${not empty teachers}">
        <table id="newspaper-b">
            <tr>
                <th>ID</th>
                <th><fmt:message key="label.name" bundle="${rb}"/></th>
                <th><fmt:message key="label.secondName" bundle="${rb}"/></th>
                <th><fmt:message key="label.email" bundle="${rb}"/></th>
            </tr>
            <c:forEach items="${teachers}" var="teacher">
                <tr>
                    <td><c:out value="${teacher.id}"/></td>
                    <td><c:out value="${teacher.name}"/></td>
                    <td><c:out value="${teacher.secondName}"/></td>
                    <td><c:out value="${teacher.email}"/></td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
    <c:if test="${role == 'ADMINISTRATOR'}">
        <input type="submit" form="new-teacher" style="margin-bottom: 30px" class="btn btn-info" value=<fmt:message key="label.add-teacher" bundle="${rb}"/>>
    </c:if>
</div>

</body>
</html>
