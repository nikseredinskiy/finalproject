<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 28.11.2015
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<html>
<head>
    <title><fmt:message key="label.archive-list" bundle="${rb}" /></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="add-student">
    <form></form>
    <c:if test="${not empty archive}">
        <table id="newspaper-b">
            <tr>
                <th><fmt:message key="label.name" bundle="${rb}" /></th>
                <th><fmt:message key="label.secondName" bundle="${rb}" /></th>
                <th><fmt:message key="label.email" bundle="${rb}" /></th>
                <th><fmt:message key="label.university" bundle="${rb}" /></th>
            </tr>
            <c:forEach items="${archive}" var="student">
                <tr>
                    <td><c:out value="${student.name}"/></td>
                    <td><c:out value="${student.secondName}"/></td>
                    <td><c:out value="${student.email}"/></td>
                    <td><c:out value="${student.university}"/></td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>
</body>
</html>
