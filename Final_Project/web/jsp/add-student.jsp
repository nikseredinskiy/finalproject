<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 04.11.2015
  Time: 15:25
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><fmt:message key="label.add-student" bundle="${rb}"/></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="../js/checkPassword.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<c:import url="header.jsp"/>
<div class="container">
    <div id="login-form">
        <h3><fmt:message key="label.registration" bundle="${rb}"/></h3>
        <fieldset>
            <form action="/controller" method="post" id="student-form">
                <input type="hidden" name="command" value="add-student">

                <div class="control-label"><fmt:message key="label.name" bundle="${rb}"/>:</div>
                <input type="text" name="name" pattern="[a-zA-Z ]{3,30}" required>
                <br/>

                <div class="control-label"><fmt:message key="label.secondName" bundle="${rb}"/>:</div>
                <input type="text" name="secondName" pattern="[a-zA-Z ]{3,30}" required>
                <br/>

                <div class="control-label"><fmt:message key="label.email" bundle="${rb}"/>:</div>
                <input type="email" name="email" required>
                <br/>

                <div class="control-label"><fmt:message key="label.password" bundle="${rb}"/>:</div>
                <input type="password" name="password" name="pass1" id="pass1" onkeyup="checkPass(); return false;"
                       required>
                <br/>

                <div class="control-label"><fmt:message key="label.confirm-password" bundle="${rb}"/>:</div>
                <input type="password" name="password" name="pass2" id="pass2" onkeyup="checkPass(); return false;">
                <br/>

                <div class="control-label"><fmt:message key="label.university" bundle="${rb}"/>:</div>
                <input type="text" name="university" pattern="[a-zA-Z ]{2,30}" required>
                <br/>

                <div class="control-label"><fmt:message key="label.course" bundle="${rb}"/>:</div>
                <input type="text" name="course" pattern="[1-9 ]{1,2}"required>
                <br/>
            </form>
            <form action="/controller" method="post" id="back-form">
                <input type="hidden" name="command" value="back">
            </form>
            <input type="submit" form="back-form" style="float: none;" class="btn btn-danger"
                   value=<fmt:message key="label.back" bundle="${rb}"/>>
            <input type="submit" form="student-form" class="btn btn-default" value= <fmt:message key="label.create-student"
                                                                             bundle="${rb}"/>>
        </fieldset>
    </div>
</div>
<c:import url="footer.jsp" />
</body>
</html>
