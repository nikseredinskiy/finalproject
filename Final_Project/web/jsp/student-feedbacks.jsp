<%--
  Created by IntelliJ IDEA.
  User: Nik
  Date: 28.11.2015
  Time: 11:04
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="pagecontent" var="rb"/>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><fmt:message key="label.feedback" bundle="${rb}" /></title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<c:import url="header.jsp"/>
<div class="container">
    <hr>
    <div id="add-student">
        <c:if test="${not empty studentFeedbacks}">
            <table id="newspaper-b">
                <tr>
                    <th>ID</th>
                    <th><fmt:message key="label.name" bundle="${rb}" /></th>
                    <th><fmt:message key="label.feedback" bundle="${rb}" /></th>
                    <th><fmt:message key="label.mark" bundle="${rb}" /></th>
                </tr>
                <c:forEach items="${studentFeedbacks}" var="feedback">
                    <tr>
                        <td><c:out value="${feedback.id}" /></td>
                        <td><c:out value="${feedback.trainingName}" /></td>
                        <td><c:out value="${feedback.feedback}" /></td>
                        <td><c:out value="${feedback.mark}" /></td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
        <form action="/controller" method="post" id="back-form">
            <input type="hidden" name="command" value="back">
        </form>
        <input type="submit" form="back-form" style="float: right;" class="btn btn-danger"
               value=<fmt:message key="label.back" bundle="${rb}"/>>
    </div>
</div>
<c:import url="footer.jsp" />
</body>
</html>
